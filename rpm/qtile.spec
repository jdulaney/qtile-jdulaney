Summary: A pure-Python tiling window manager
Name: qtile
Version: 0.8.0
Release: 1%{?dist}
Source0: https://github.com/qtile/qtile/archive/v%{version}.tar.gz
License: MIT
BuildArch: noarch
Url: http://qtile.org

Source1:  qtile.desktop

BuildRequires:  python2-devel
BuildRequires:  python-setuptools
BuildRequires:  desktop-file-utils
BuildRequires:  xorg-x11-server-Xephyr
BuildRequires:  python-cffi
BuildRequires:  python-nose-cov
BuildRequires:  python-trollius
BuildRequires:  python-xcffib
BuildRequires:  python-six
BuildRequires:  python-cairocffi

Requires:  python-cairocffi
Requires:  python-xpyb
Requires:  python-xcffib
Requires:  python-trollius

%description

A pure-Python tiling window manager.

Features
========

    * Simple, small and extensible. It's easy to write your own layouts,
      widgets and commands.
    * Configured in Python.
    * Command shell that allows all aspects of
      Qtile to be managed and inspected.
    * Complete remote scriptability - write scripts to set up workspaces,
      manipulate windows, update status bar widgets and more.
    * Qtile's remote scriptability makes it one of the most thoroughly
      unit-tested window mangers around.


%prep
%setup -q -n qtile-%{version} 

%build
%{__python2} setup.py build

%install
%{__python2} setup.py install --single-version-externally-managed -O1 --root=$RPM_BUILD_ROOT --record=INSTALLED_FILES
mkdir -p %{buildroot}%{_datadir}/xsessions/
install -m 644 %{SOURCE1} %{buildroot}%{_datadir}/xsessions/

desktop-file-validate %{buildroot}%{_datadir}/xsessions/qtile.desktop


%files
%doc LICENSE
%doc README.rst
%{_mandir}/man1/qtile.1.gz
%{_mandir}/man1/qsh.1.gz
%{_bindir}/qsh
%{_bindir}/qtile
%{_bindir}/qtile-run
%{_bindir}/qtile-session
%{python2_sitelib}/qtile-%{version}-py2.7.egg-info/*
%{python2_sitelib}/libqtile/*
%{_datadir}/xsessions/qtile.desktop


%changelog
* Wed Oct 08 2014 John Dulaney <jdulaney@fedoraproject.org> - 0.8.0-1
- Initial packaging
- Spec based on python-nose
